#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from TECNO-BF7h device
$(call inherit-product, device/tecno/TECNO-BF7h/device.mk)

PRODUCT_DEVICE := TECNO-BF7h
PRODUCT_NAME := lineage_TECNO-BF7h
PRODUCT_BRAND := TECNO
PRODUCT_MODEL := TECNO BF7h
PRODUCT_MANUFACTURER := tecno

PRODUCT_GMS_CLIENTID_BASE := android-transsion

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="sys_tssi_32_ago_tecno-user 12 SP1A.210812.016 196940 release-keys"

BUILD_FINGERPRINT := TECNO/BF7h-AS/TECNO-BF7h:12/SP1A.210812.016/221220V491:user/release-keys
