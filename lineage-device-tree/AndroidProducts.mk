#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_TECNO-BF7h.mk

COMMON_LUNCH_CHOICES := \
    lineage_TECNO-BF7h-user \
    lineage_TECNO-BF7h-userdebug \
    lineage_TECNO-BF7h-eng
